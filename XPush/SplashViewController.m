//
//  SplashViewController.m
//  XPush
//
//  Created by notdol on 3/22/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(showView:) userInfo:nil repeats:NO];
}

- (void)showView:(NSTimer *) timer{
    xpushViewController *mainView = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninSB"];
    [self.navigationController pushViewController:mainView animated:YES];
    //self present
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
