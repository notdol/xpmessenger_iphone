//
//  XPRoomViewController.m
//  XPush
//
//  Created by notdol on 3/8/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import "XPRoomViewController.h"
#import "ChannelInfo.h"
#import "XPChatViewController.h"
#import "XPushClient.h"
#import "xpushAppDelegate.h"
#import "StalkIOAppDelegate.h"
#import "TDBadgedCell.h"

@interface XPRoomViewController ()

@end

@implementation XPRoomViewController
@synthesize xpush;
@synthesize channelList;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    xpush = [XPushClient getInstance];
    channelList = [xpush _channelList];
    NSLog(@"view Did load channelList count : %lu", (unsigned long)[channelList count]);
    [xpush setDelegate:self];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return channelList.count;
}

-(void) receivedNewChannel:(ChannelInfo *)ch{
    NSLog(@"normally received new channel=========");
    
	xpushAppDelegate *appDelegate = (xpushAppDelegate *)[[UIApplication sharedApplication] delegate];
    StalkIOAppDelegate *stalk = appDelegate.stalk;
    if([stalk insertChannel:ch]){
        [channelList addObject:ch];
        [[self tableView] reloadData];
    }
    
    NSLog(@"channelList count : %lu", (unsigned long)[channelList count]);
}

-(void) receivedMessage:(NSString *)channel msgObj:(XPushMessage *)msgObj{
    NSLog(@" XPRoom receivedMessage : %@",msgObj);
    [self plusChannelBadge:channel];
    [self addNotification:msgObj.message];
    [[self tableView] reloadData];
}


-(void) plusChannelBadge:(NSString *)chID{
    
    for(ChannelInfo *ch in channelList){
        if([ch.name isEqualToString:chID]){
            ch.msgCount++;
        }
    }
}

- (void)addNotification:(NSString *) msg {
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    localNotification.fireDate = [NSDate date];
    localNotification.alertBody = msg;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = 1;
    
    NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:@"Object 1", @"Key 1", @"Object 2", @"Key 2", nil];
    localNotification.userInfo = infoDict;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    TDBadgedCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [[TDBadgedCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        /*
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
         */
    }
    
    //NSLog(@"cell data is : %@",[_channelList objectAtIndex:indexPath.row]);
    ChannelInfo *chInfo = [channelList objectAtIndex:indexPath.row];    
    NSLog(@"row key is : %@", chInfo.name);
    cell.textLabel.text = chInfo.name;
    
    if(chInfo.msgCount > 0 ){
        cell.badgeString = [NSString stringWithFormat:@"%d",chInfo.msgCount];
    }else{
        cell.badgeString = nil;
    }

    /*
    static NSString *CellIdentifier = @"NormalFriend";
    
    XPFriend *currentFriend = [self.friends objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    cell.textLabel.text = currentFriend.name;
    
    return cell;
    */
    // Configure the cell...
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"=============== segue : %@",[segue identifier]);
    if ([[segue identifier] isEqualToString:@"chatSegue"])
    {
        
        NSIndexPath *path = [[self tableView] indexPathForSelectedRow];
        ChannelInfo *ch = [channelList objectAtIndex:path.row];
        
        XPChatViewController *vc = [segue destinationViewController];
        vc.channel = ch.name; //SecondView의 member 임.
        ch.msgCount = 0;
        [[self tableView]reloadData];
    }
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
