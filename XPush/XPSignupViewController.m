//
//  XPSignupViewController.m
//  XPush
//
//  Created by notdol on 3/8/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import "XPSignupViewController.h"
#import "xpushViewController.h"

@interface XPSignupViewController ()

@end

@implementation XPSignupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signupBtnClicked:(id)sender {
    
    
}

- (IBAction)signinBtnClicked:(id)sender {
    xpushViewController *signinVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SigninSB"];
    [self presentViewController:signinVC animated:NO completion:nil];
}
@end
