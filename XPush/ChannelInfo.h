//
//  ChannelInfo.h
//  XPush
//
//  Created by notdol on 3/8/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChannelInfo : NSData

@property (nonatomic,strong) NSString *name;
@property (nonatomic,assign) NSInteger count;
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,assign) NSInteger msgCount;

@end
