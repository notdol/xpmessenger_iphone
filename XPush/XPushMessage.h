//
//  XPushMessage.h
//  XPush
//
//  Created by notdol on 3/16/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XPushMessage : NSObject
@property (nonatomic,strong) NSString *sender;
@property (nonatomic,strong) NSString *message;
@end
