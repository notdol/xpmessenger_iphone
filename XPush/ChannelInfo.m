//
//  ChannelInfo.m
//  XPush
//
//  Created by notdol on 3/8/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import "ChannelInfo.h"

@implementation ChannelInfo

@synthesize name;
@synthesize count;
@synthesize id;
@synthesize msgCount;

-(id) init{
    self = [super init];
    if(self){
        self.msgCount = 0;
    }
    
    return self;
}

@end
