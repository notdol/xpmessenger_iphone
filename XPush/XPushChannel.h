//
//  XPushChannel.h
//  XPush
//
//  Created by notdol on 3/9/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketIO.h"
#import "XPushMessage.h"

@protocol XPushChannelDelegate <NSObject>
@optional
- (void) receivedMessage:(XPushMessage *)message;
@end


@interface XPushChannel : NSObject <SocketIODelegate>{
    __weak id<XPushChannelDelegate> _delegate;
}

@property (nonatomic, strong) NSString *_name;
@property (nonatomic, strong) NSString *_host;
@property (nonatomic, strong) NSString *_port;
@property (nonatomic, strong) NSString *_chName;
@property (nonatomic, strong) SocketIO *_socketIO;

-(void)setDelegate:(id<XPushChannelDelegate>)delegate;
- (void) socketIODidConnect:(SocketIO *)socket;
- (void) socketIO:(SocketIO *)socket didReceiveMessage:(SocketIOPacket *)packet;
- (void) socketIO:(SocketIO *)socket didReceiveJSON:(SocketIOPacket *)packet;
- (void) socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet;


-(id) initWithNameAndUrl:(NSString *)name url:(NSString *)url;
- (void) disconnect;
@end
