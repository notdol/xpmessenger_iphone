//
//  XPChatViewController.h
//  XPush
//
//  Created by notdol on 3/8/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XPushChannel.h"
#import "UIBubbleTableViewDataSource.h"

@interface XPChatViewController : UIViewController <UIBubbleTableViewDataSource, XPushChannelDelegate>
//@property (weak, nonatomic) IBOutlet UILabel *txtField;
@property (nonatomic, strong) NSString *channel;
@property (nonatomic, strong) XPushChannel * node;

- (void) receivedMessage:(XPushMessage *)message;
@end
