//
//  SplashViewController.h
//  XPush
//
//  Created by notdol on 3/22/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "xpushViewController.h"

@interface SplashViewController : UIViewController
- (void)showView;

@end
