//
//  xpushViewController.h
//  XPush
//
//  Created by notdol on 3/4/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XPushClient.h"

@interface xpushViewController : UIViewController <XPushSessionDelegate>
- (void) didReceiveFinished:(NSDictionary *)params;
@property (weak, nonatomic) IBOutlet UITextField *idTxtField;
@property (weak, nonatomic) IBOutlet UITextField *passwdField;
- (IBAction)signinBtnClicked:(UIButton *)sender;
- (IBAction)signupBtnClicked:(UIButton *)sender;
-(void) connectComplete;
@end
