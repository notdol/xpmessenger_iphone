//
//  StalkIOAppDelegate.m
//  XPush
//
//  Created by notdol on 3/16/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import "StalkIOAppDelegate.h"
#import "ChannelInfo.h"

@implementation StalkIOAppDelegate
//@synthesize window;
//@synthesize navigationController;

@synthesize DBName, DBPath, DBData, isFirstTimeAccess;

@synthesize DBChannelName, DBMessageName; // 데이터 베이스 이름

- (BOOL)initWithDB {
    
    // Override point for customization after application launch.
    NSLog(@"++++++++++++++++++ didFinishLaunchingWithOptions");
	// 최초 엑세스 인가?
	self.isFirstTimeAccess = YES;
	
	// 앱의 파일 시스템 경로를 구한다.
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	
	// 데인터베이스 이름과 경로를 저장한다.
	self.DBChannelName = @"channel.db";
	self.DBMessageName = @"message.db";    
	self.DBChannelPath = [documentsDir stringByAppendingPathComponent:self.DBChannelName];
	self.DBMessagePath = [documentsDir stringByAppendingPathComponent:self.DBMessageName];
	NSLog(@"path ==== %@",self.DBChannelPath);
    NSLog(@"path ==== %@",self.DBMessagePath);
	[self checkAndCreateDatabase];
	
	//[self readMemoFromDatabase];
	
    // Add the navigation controller's view to the window and display.
    //[self.window addSubview:navigationController.view];
    //[self.window makeKeyAndVisible];
    
    return YES;
}

- (void)checkAndCreateDatabase {
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	// 해당 경로에 데이터 베이스 파일이 존재하는지 확인한다.
	if ([fileManager fileExistsAtPath:self.DBChannelPath] && [fileManager fileExistsAtPath:self.DBMessagePath]) {
		return;
	}
	
	// 데이터 베이스 파일이 없다면 번들(앱에 포함된 리소스)에서 데이터 베이스 파일을 복사한다.
	else {
        NSLog(@"+++++++++++++++++++++++++++");
        NSLog(@"%@",[[NSBundle mainBundle] bundlePath]);
		NSString *databasePathFromApp1 = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:self.DBChannelName];
		[fileManager copyItemAtPath:databasePathFromApp1 toPath:self.DBChannelPath error:nil];
        
		NSString *databasePathFromApp2 = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:self.DBMessageName];
		[fileManager copyItemAtPath:databasePathFromApp2 toPath:self.DBMessagePath error:nil];
		//[fileManager release];
	}
}

-(Boolean)insertChannel:(ChannelInfo *)channel{
    Boolean isSuccess = FALSE;
    sqlite3 *database;
    sqlite3_stmt *stmt;
    const char *sqlStatement = "INSERT INTO tblChannel(XP_Id, XP_Name, XP_Count) VALUES(?,?,?)";
    
    const char *dbPath = [self.DBChannelPath UTF8String];
    if(sqlite3_open(dbPath,&database)==SQLITE_OK) {
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &stmt, NULL)==SQLITE_OK) {
            
            sqlite3_bind_int(stmt, 1, channel.id);
            sqlite3_bind_text(stmt, 2, [channel.name UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(stmt, 3, channel.count);
            
			if (SQLITE_DONE != sqlite3_step(stmt)) {
				NSAssert1(0, @"Error while inserting into tblMessage. '%s'", sqlite3_errmsg(database));
			}
			sqlite3_reset(stmt);
            isSuccess = TRUE;
            NSLog(@"i-%d / %d / %@ ", channel.id, channel.count, channel.name);
            
			// 데이터베이스를 닫는다.
			sqlite3_close(database);
        }else {
			printf("could not prepare statemnt: %s\n", sqlite3_errmsg(database));
		}
        //sqlite3_finalize(stmt);
    }else {
		sqlite3_close(database);
		NSAssert1(0, @"Error opening database in tblMemoPad. '%s'", sqlite3_errmsg(database));
	}
    return isSuccess;
    //sqlite3_close(database);
}

-(Boolean)insertMessage:(NSString *)channelId message:(XPushMessage *)msgObj{
    Boolean isSuccess = FALSE;
    sqlite3 *database;
    sqlite3_stmt *stmt;
    const char *sqlStatement = "INSERT INTO tblMessage(XP_Sender, XP_Message, XP_ChannelR) VALUES(?,?,?)";
    
    const char *dbPath = [self.DBMessagePath UTF8String];
    if(sqlite3_open(dbPath,&database)==SQLITE_OK) {
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &stmt, NULL)==SQLITE_OK) {
            
            sqlite3_bind_text(stmt, 1, [msgObj.sender UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 2, [msgObj.message UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt, 3, [channelId UTF8String], -1, SQLITE_TRANSIENT);
            
			if (SQLITE_DONE != sqlite3_step(stmt)) {
				NSAssert1(0, @"Error while inserting into tblMessage. '%s'", sqlite3_errmsg(database));
			}
			sqlite3_reset(stmt);
            isSuccess = TRUE;
			// 데이터베이스를 닫는다.
			sqlite3_close(database);
        }else {
			printf("could not prepare statemnt: %s\n", sqlite3_errmsg(database));
		}
        //sqlite3_finalize(stmt);
    }else {
		sqlite3_close(database);
		NSAssert1(0, @"Error opening database in tblMemoPad. '%s'", sqlite3_errmsg(database));
	}
    return isSuccess;
    //sqlite3_close(database);
}


- (NSMutableArray *)readChannel {
	NSMutableArray *channelList = [[NSMutableArray alloc]init];
	sqlite3 *database;
	
	// 데이터를 담을 오브젝트인 DBData를 초기화한다.
	// 데이터베이스에 처음 엑세스하는 것이라면 alloc으로 메모리를 할당한다.
	if (self.isFirstTimeAccess == YES) {
		self.DBData = [[NSMutableArray alloc] init];
		self.isFirstTimeAccess == NO;
	}
	
	// 처음 엑세스하는 것이 아니면 removeAllObject로 DBData를 초기화 한다.
	else {
		[self.DBData removeAllObjects];
	}
	
	// 데이터 베이스 파일을 연다.
	if (sqlite3_open([self.DBChannelPath UTF8String], &database) == SQLITE_OK) {
		
		// SQL명령을 실행한다.
		const char *sqlStatement = "SELECT * FROM tblChannel";
		sqlite3_stmt *compiledStatement;
        
		if (sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			
			// SQL 명령어를 실행하여 얻은 결과를 하나씩 읽는다.
			while (sqlite3_step(compiledStatement) == SQLITE_ROW) {				
				NSInteger aIndex = sqlite3_column_int(compiledStatement, 0);
				NSInteger id = sqlite3_column_int(compiledStatement, 1);
                
				NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                NSInteger count = sqlite3_column_int(compiledStatement, 3);
                
                // 읽어온 데이터를 사용하여 MemoData 오브젝트를 생성한다.
                ChannelInfo *channel = [[ChannelInfo alloc]init];
                channel.id = id;
                channel.name = name;
                channel.count = count;
                [channelList addObject:channel];
                
                NSLog(@"r-%d / %d / %@ ", id, count, name);
                /*
                MemoData *md = [[MemoData alloc] initWithData:aIndex Title:aTitle Content:aContent Date:aDate];
                NSLog(@"%d / %@ / %@ / %@", aIndex, aTitle, aContent, aDate);
                // MemoData 오브젝트를 DBData 배열에 추가한다.
                [self.DBData addObject:md];
                [md release];
                 */
			}
		}
		
		else {
			printf("could not prepare statement: %s\n", sqlite3_errmsg(database));
		}
		
		// SQL 명령을 종료한다.
		sqlite3_finalize(compiledStatement);
	}
	
	// 데이터베이스를 닫는다.
	sqlite3_close(database);
    return channelList;
}

- (NSMutableArray *)readMessage:(NSString *)channel {
	NSMutableArray *messageList = [[NSMutableArray alloc]init];
	sqlite3 *database;
	
	// 데이터를 담을 오브젝트인 DBData를 초기화한다.
	// 데이터베이스에 처음 엑세스하는 것이라면 alloc으로 메모리를 할당한다.
	if (self.isFirstTimeAccess == YES) {
		self.DBData = [[NSMutableArray alloc] init];
		self.isFirstTimeAccess == NO;
	}
	
	// 처음 엑세스하는 것이 아니면 removeAllObject로 DBData를 초기화 한다.
	else {
		[self.DBData removeAllObjects];
	}
	
	// 데이터 베이스 파일을 연다.
	if (sqlite3_open([self.DBMessagePath UTF8String], &database) == SQLITE_OK) {
		
		// SQL명령을 실행한다.
		const char *sqlStatement = "SELECT * FROM tblMessage WHERE XP_ChannelR = ?";
		sqlite3_stmt *compiledStatement;
        
		if (sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			
            sqlite3_bind_text(compiledStatement, 1, [channel UTF8String], -1, SQLITE_TRANSIENT);
            
			// SQL 명령어를 실행하여 얻은 결과를 하나씩 읽는다.
			while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
				NSInteger aIndex = sqlite3_column_int(compiledStatement, 0);
				NSString *sender = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
				NSString *message = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                NSString *channel = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                // 읽어온 데이터를 사용하여 MemoData 오브젝트를 생성한다.
                XPushMessage *msgObj = [[XPushMessage alloc]init];
                msgObj.sender = sender;
                msgObj.message = message;
                [messageList addObject:msgObj];
                
                NSLog(@"r-%@ / %@ / %@ ", sender, message, channel);
                /*
                 MemoData *md = [[MemoData alloc] initWithData:aIndex Title:aTitle Content:aContent Date:aDate];
                 NSLog(@"%d / %@ / %@ / %@", aIndex, aTitle, aContent, aDate);
                 // MemoData 오브젝트를 DBData 배열에 추가한다.
                 [self.DBData addObject:md];
                 [md release];
                 */
			}
		}
		
		else {
			printf("could not prepare statement: %s\n", sqlite3_errmsg(database));
		}
		
		// SQL 명령을 종료한다.
		sqlite3_finalize(compiledStatement);
	}
	
	// 데이터베이스를 닫는다.
	sqlite3_close(database);
    return messageList;
}




@end
