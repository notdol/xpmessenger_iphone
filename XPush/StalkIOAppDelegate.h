//
//  StalkIOAppDelegate.h
//  XPush
//
//  Created by notdol on 3/16/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "ChannelInfo.h"
#import "XPushMessage.h"

@interface StalkIOAppDelegate : NSObject{
    //UIWindow *window;
    //UINavigationController *navigationController;
	
	NSString *DBName; // 데이터 베이스 이름
	NSString *DBChannelName; // 데이터 베이스 이름
	NSString *DBMessageName; // 데이터 베이스 이름
    
	NSString *DBPath; // 데이터 베이스 경로
	NSString *DBChannelPath; // 데이터 베이스 경로
	NSString *DBMessagePath; // 데이터 베이스 경로
	NSMutableArray *DBData; // 데이터 베이스에서 읽은 데이터를 가져온다.
    
	BOOL isFirstTimeAccess; // 데이터 베이스에 처음으로 접속하는지 여부
}

//@property (nonatomic, retain) IBOutlet UIWindow *window;
//@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

@property (nonatomic, retain) NSString *DBName;
@property (nonatomic, retain) NSString *DBChannelName;
@property (nonatomic, retain) NSString *DBMessageName;

@property (nonatomic, retain) NSString *DBPath;
@property (nonatomic, retain) NSString *DBChannelPath;
@property (nonatomic, retain) NSString *DBMessagePath;
@property (nonatomic, retain) NSMutableArray *DBData;
@property (nonatomic, assign) BOOL isFirstTimeAccess;

- (BOOL)initWithDB;
- (void)checkAndCreateDatabase; // 파일이 있는지 체크하고 없으면 생성하는 메소드
- (Boolean)insertChannel:(ChannelInfo *)channel;
- (Boolean)insertMessage:(NSString *)channelId message:(XPushMessage *)msgObj;
- (NSMutableArray *)readChannel;
- (NSMutableArray *)readMessage:(NSString *)channel;

@end
