//
//  HTTPRequest.h
//  XPush
//
//  Created by notdol on 3/7/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTTPRequest : NSObject
{
    NSMutableData *receivedData;
    NSURLResponse *response;
    NSDictionary *result;
    id target;
    SEL selector;
}

- (BOOL)requestUrl:(NSString *)url bodyObject:(NSDictionary *)bodyObject;
- (BOOL)requestUrl:(NSString *)url bodyObject:(NSDictionary *)bodyObject method:(NSString *)method;
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse;
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
- (void)setDelegate:(id)aTarget selector:(SEL)aSelector;

@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, strong) NSDictionary *result;
@property (nonatomic, strong) id target;
@property (nonatomic, assign) SEL selector;


@end
