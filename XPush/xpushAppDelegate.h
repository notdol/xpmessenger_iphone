//
//  xpushAppDelegate.h
//  XPush
//
//  Created by notdol on 3/4/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StalkIOAppDelegate.h"

@interface xpushAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) StalkIOAppDelegate *stalk;

@end
