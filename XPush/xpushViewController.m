//
//  xpushViewController.m
//  XPush
//
//  Created by notdol on 3/4/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import "xpushViewController.h"
#import "HTTPRequest.h"
#import "XPSignupViewController.h"
#import "XPRoomViewController.h"
#import "XPMainViewController.h"

@interface xpushViewController ()

@end

@implementation xpushViewController

@synthesize idTxtField;
@synthesize passwdField;

NSString *sessionUrl = @"http://stalk.xpush.io:8000";
NSString *appId = @"stalk-io:asdfeeee";

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"this is view did load");    
//    [window makeKeyAndVisible];
    
    idTxtField.text = @"james@xpush.com";
    passwdField.text = @"111111";
}

- (void)socketConnected{
    [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"mainSB"] animated:NO completion:nil];
}

- (void) didReceiveFinished:(NSDictionary *)params{
    NSLog(@"this is response");
    NSLog(params);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signinBtnClicked:(UIButton *)sender {
    
    HTTPRequest *httpRequest = [[HTTPRequest alloc] init];
    
    NSString *userId = idTxtField.text;
    NSString *password = passwdField.text;
    NSString *deviceId = @"35424905084289";//[UIDevice description];
    
    NSDictionary *bodyObject = [NSDictionary dictionaryWithObjectsAndKeys:@"stalk-io:asdfeeee",@"app",userId,@"userId",password,@"password", deviceId,@"deviceId", nil];
    
    [httpRequest setDelegate:self selector:@selector(signinSubmit:)];
    [httpRequest requestUrl:[sessionUrl stringByAppendingString:@"/auth"] bodyObject:bodyObject];
}

- (void) signinSubmit:(NSDictionary *)data{
    NSLog(@"this is /auth return data %@",data[@"result"][@"serverUrl"]);
    //NSLog([data objectForKey:@"serverUrl"]);
    
    XPushClient *client = [XPushClient getInstance];//[[XPushClient alloc] init];
    
    [client setSessionServerUrl:data[@"result"][@"serverUrl"] ];
    [client setInformation:@"stalk-io:asdfeeee" userId:@"james@xpush.com" deviceId:@"35424905084289" token:data[@"result"][@"token"]];
    
    [client setDelegate:self];
    
    [client connect];
    //[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(connectComplete:) userInfo:nil repeats:NO];
    
    //[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]
}

-(void) connectComplete{
    XPMainViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mainSB"];
    
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [vc setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self.navigationController presentViewController:vc animated:YES completion:nil ];
    //[self presentViewController:vc animated:YES completion:nil];

}

- (IBAction)signupBtnClicked:(UIButton *)sender {
     XPSignupViewController *signupVC = [self.storyboard instantiateViewControllerWithIdentifier:@"signupStoryboard"];
    [self presentViewController:signupVC animated:NO completion:nil];
    
    
}
@end
