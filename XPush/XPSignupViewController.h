//
//  XPSignupViewController.h
//  XPush
//
//  Created by notdol on 3/8/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XPSignupViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *idField;
@property (weak, nonatomic) IBOutlet UITextField *passwd1Field;
@property (weak, nonatomic) IBOutlet UITextField *passwd2Field;
- (IBAction)signupBtnClicked:(id)sender;
- (IBAction)signinBtnClicked:(id)sender;

@end
