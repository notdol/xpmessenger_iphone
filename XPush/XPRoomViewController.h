//
//  XPRoomViewController.h
//  XPush
//
//  Created by notdol on 3/8/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XPushClient.h"
@interface XPRoomViewController : UITableViewController <XPushSessionDelegate>

@property (nonatomic, weak) XPushClient *xpush;
@property (nonatomic, weak) NSMutableArray *channelList;

-(void) receivedNewChannel:(ChannelInfo *)ch;
- (void)addNotification:(NSString *) msg;
-(void) receivedMessage:(NSString *)channel msgObj:(XPushMessage *)msgObj;
@end
