//
//  XPushChannel.m
//  XPush
//
//  Created by notdol on 3/9/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import "XPushChannel.h"
#import "HTTPRequest.h"
#import "SocketIOPacket.h"
#import "XPushMessage.h"

NSString *channelNamespace = @"/channel";

@implementation XPushChannel
@synthesize _name;
@synthesize _host;
@synthesize _port;
@synthesize _socketIO;
@synthesize _chName;

-(void)setDelegate:(id<XPushChannelDelegate>)delegate{
    _delegate = delegate;
}

-(id) initWithNameAndUrl:(NSString *)name url:(NSString *)url{
    self = [super init];
    NSLog(@" XPushChannel initWithNameAndUrl : %@ , %@",name,url);
    if(self){
        _name = name;
        //NSArray* list= [url componentsSeparatedByString:@":"];
        //_host = [[list objectAtIndex:1] substringFromIndex:2];
        //_port = [list objectAtIndex:2];
        [self getChannelNodeInfo];
    }

    return self;
}

-(void)getChannelNodeInfo{
    NSString *sessionUrl = @"http://stalk.xpush.io:8000";
    NSString *appId = @"stalk-io:asdfeeee";//@"stalk-io:asdfeeee";
    HTTPRequest *httpRequest = [[HTTPRequest alloc] init];
    NSString *context = [@"/node/" stringByAppendingString:[self stringEncodeURIComponent:appId]];
    context = [[context stringByAppendingFormat:@"/"] stringByAppendingString:[_name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ];
    [httpRequest setDelegate:self selector:@selector(receivedServerInfo:)];
    NSString *url = [sessionUrl stringByAppendingString:context];
    NSLog(@" channel request info : %@ ",url);
    [httpRequest requestUrl:url bodyObject:nil method:@"GET"];
}

-(NSString*)stringEncodeURIComponent:(NSString *)string {
    
    int strLength = 0;
    NSLog(@" urlStr : %@", string );
    NSMutableString *mutableUrlStr = [string mutableCopy];
    NSLog(@" mutableUrlStr : %@", mutableUrlStr );
    strLength = [mutableUrlStr length];
    [mutableUrlStr replaceOccurrencesOfString:@":" withString:@"%3A" options:NSCaseInsensitiveSearch range:NSMakeRange(0, strLength)];
    NSLog(@" mutableUrlStr : %@", mutableUrlStr );
    strLength = [mutableUrlStr length];
    [mutableUrlStr replaceOccurrencesOfString:@"/" withString:@"%2F" options:NSCaseInsensitiveSearch range:NSMakeRange(0, strLength)];
    NSLog(@" mutableUrlStr : %@", mutableUrlStr );
    return mutableUrlStr;
    /*
    return CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                     (__bridge CFStringRef)string,
                                                                     NULL,
                                                                     CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                    kCFStringEncodingUTF8));
     */
}

- (void) didReceiveFinished:(NSDictionary *)params{
    NSLog(@"this is response");
    NSLog(params);
}


- (void) receivedServerInfo:(NSDictionary *)data{
    NSLog(@"this is /node return data %@",data);
    
    if([data[@"status"] isEqualToString:@"ok"]){
        NSDictionary *result = data[@"result"];
        _chName = result[@"server"][@"name"];
        NSString *url = result[@"server"][@"url"];
        NSArray* list= [url componentsSeparatedByString:@":"];
        _host = [[list objectAtIndex:1] substringFromIndex:2];
        _port = [list objectAtIndex:2];
        [self connect];
    }
    
}


-(void) connect{
    NSLog(@"Connect to socket io : %@", _name);
    _socketIO = [[SocketIO alloc] initWithDelegate:self];
    
    
    NSString *appId = [self stringEncodeURIComponent:@"stalk-io:asdfeeee" ];
    NSString *name = [_name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *sendData = [[NSDictionary alloc]initWithObjectsAndKeys:appId,@"app",name,@"channel",_chName,@"server",@"james@xpush.com",@"userId",@"35424905084289",@"deviceId", nil];
        
    NSLog(@" this is XPushchannel : %@",channelNamespace);
    [_socketIO connectToHost:_host onPort:[_port intValue] withParams:sendData withNamespace:channelNamespace];
}

- (void) disconnect{
    NSLog(@"force disconnected !!!");
    [_socketIO disconnectForced];
}


# pragma mark -
# pragma mark socket.IO-objc delegate methods

- (void) socketIODidConnect:(SocketIO *)socket
{
    NSLog(@"socket.io connected.");    
    
    //    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(creteChannel:) userInfo:nil repeats:NO];
    //[NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(getChannelList) userInfo:nil repeats:NO];
    
    //[self getChannelList];
    //[self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"mainSB"] animated:NO completion:nil];
}

- (void) socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet
{
    NSLog(@"didReceiveEvent()");
    
    NSDictionary *dic = [packet dataAsJSON];
    
    NSLog(@"current channel received : %@",dic);
    NSString *name = [dic objectForKey:@"name"];
    
    if([name isEqualToString:@"message"]){
        NSArray *args = [dic objectForKey:@"args"];
        NSLog(@"%@",args);
        NSDictionary *arg = args[0];
        NSString *message = [arg objectForKey:@"message"];
        NSString *sender = [arg objectForKey:@"sender"];
        NSLog(@"%@",message);
        NSString *chID = [arg objectForKey:@"channel"];
        NSLog(@"%@",chID);
        XPushMessage *msgObj = [[XPushMessage alloc]init];
        msgObj.message = message;
        msgObj.sender = sender;
        if(_delegate) [_delegate receivedMessage:msgObj];
    }
    
    /*
    SocketIOCallback cb = ^(id argsData) {
        NSDictionary *response = argsData;
        // do something with response
        NSLog(@"ack arrived: %@", response);
        
        // test forced disconnect
        [_socketIO disconnectForced];
    };
    [_socketIO sendMessage:@"hello back!" withAcknowledge:cb];
     */
}


- (void) socketIO:(SocketIO *)socket onError:(NSError *)error
{
    NSLog(@"onError() %@", error);
}


- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error
{
    NSLog(@"socket.io disconnected. did error occur? %@", error);
}


@end
