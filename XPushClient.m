//
//  XPushClient.m
//  XPush
//
//  Created by notdol on 3/8/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import "XPushClient.h"
#import "SocketIO.h"
#import "SocketIOPacket.h"
#import "ChannelInfo.h"
#import "XPushChannel.h"
#import "xpushAppDelegate.h"

NSString *sessionServerUrl;
NSString *sessionServerPort;

NSString *_app;
NSString *_userId;
NSString *_deviceId;
NSString *_token;
NSString *_notiId;

NSString *sessionNamespace = @"/session";

@implementation XPushClient

@synthesize _channelList;

static XPushClient *instance =nil;
+(XPushClient *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            
            instance= [XPushClient new];
        }
    }
    return instance;
}    

-(void)setDelegate:(id<XPushSessionDelegate>)delegate{
    _delegate = delegate;
}

-(void)setSessionServerUrl:(NSString *)url{
    NSLog(@"setSessionServerUrl  %@",url);
    NSArray* list= [url componentsSeparatedByString:@":"];
    sessionServerUrl = [[list objectAtIndex:1] substringFromIndex:2];
    sessionServerPort = [list objectAtIndex:2];
}

-(void)setInformation:(NSString *)app userId:(NSString *)userId deviceId:(NSString*)deviceId token:(NSString *)token{
    _app = app;
    //_app = @"stalk-io:asdfeee";
    _userId = userId;
    //_userId = @"james@xpush.com";
    _deviceId = deviceId;
    //_deviceId = @"35424905084289";
    _notiId = @"APA91bF2tNr7-I14LtfU-o1dRsadt0NQzD2dFDMiN9uLiekSL3NodsoYDpMCwYtoQ-7fysasga-GbdxhnZwrassi3OPXOiC9zsm11Ol__h3fCYQ1x_M1zohBE5f7psDO3XobjQJTShs8YBT0R-VOCXFUceoYIVSPvQ";
    _token = token;
}

-(void)connect{
    NSLog(@"Connect to socket io %@ : %@", sessionServerUrl, sessionServerPort);
    NSLog(@"socket namespace %@",sessionNamespace);
    socketIO = [[SocketIO alloc] initWithDelegate:self];
    [socketIO connectToHost:sessionServerUrl onPort:[sessionServerPort intValue] withParams:[NSDictionary dictionaryWithObjectsAndKeys:_app,@"app",_userId,@"userId",_deviceId,@"deviceId",_token,@"token", nil] withNamespace:sessionNamespace withConnectionTimeout:1000000];
    
    // is connect with socket server???
    [self socketIODidConnect:nil];
}


-(Boolean) isExistChannel:(NSString *)chNm{
    for(ChannelInfo *ch in _channelList){
        NSLog(@" compare channel  %@---%@",ch.name,chNm);
        if([ch.name isEqualToString:chNm]){
            return TRUE;
        }
    }
    return FALSE;
}

-(XPushChannel *)connectToChannel:(NSString *)name{
    XPushChannel *xpCH;
    for(ChannelInfo *ch in _channelList){
        if([ch.name isEqualToString:name]){
            xpCH = [[XPushChannel alloc] initWithNameAndUrl:ch.name url:@""];
        }
    }
    return xpCH;
};


# pragma mark -
# pragma mark socket.IO-objc delegate methods

- (void) socketIODidConnect:(SocketIO *)socket
{
    NSLog(@"socket.io connected.");
    
//    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(creteChannel:) userInfo:nil repeats:NO];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(getChannelList) userInfo:nil repeats:NO];
    [NSTimer scheduledTimerWithTimeInterval:10000 target:self selector:@selector(emptyFunction) userInfo:nil repeats:NO];
    
    //[self getChannelList];
    //[self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"mainSB"] animated:NO completion:nil];
    if(_delegate){
        [_delegate socketConnected];
    }
}

- (void) emptyFunction{
    return;
}


- (void) socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet
{
    NSLog(@"didReceiveEvent()");

    NSDictionary *dic = [packet dataAsJSON];
    NSLog(@" session is received %@", dic);
    
    NSString *name = [dic objectForKey:@"name"];
    
    if([name isEqualToString:@"_event"]){
        NSArray *args = [dic objectForKey:@"args"];
        NSLog(@"%@",args);
        NSDictionary *arg = args[0];
        NSString *event = [arg objectForKey:@"event"];
        NSLog(@"%@",event);
        NSDictionary *data = [arg objectForKey:@"data"];
                
        NSString *chID = [data objectForKey:@"channel"];
        NSString *message = [data objectForKey:@"message"];
        NSString *sender = [data objectForKey:@"sender"];
        
        NSLog(@"%@",chID);
        ChannelInfo *newChannel = [[ChannelInfo alloc]init];
        newChannel.name = chID;
    
        if([event isEqualToString:@"NOTIFICATION"]){
           if( [self isExistChannel:chID] == FALSE){
               NSLog(@"=========== new channel ");
               if(_delegate){
                   [_delegate receivedNewChannel:newChannel];
               }
           }
            XPushMessage *msgObj = [[XPushMessage alloc]init];
            msgObj.message = message;
            msgObj.sender = sender;
            if(_delegate) [_delegate receivedMessage:chID msgObj:msgObj];
        }
    }
    SocketIOCallback cb = ^(id argsData) {
        NSDictionary *response = argsData;
        // do something with response
        NSLog(@"ack arrived: %@", response);
        
        // test forced disconnect
        //`[socketIO disconnectForced];
    };
    //[socketIO sendMessage:@"hello back!" withAcknowledge:cb];
}

- (void) socketIO:(SocketIO *)socket didReceiveMessage:(SocketIOPacket *)packet{
    NSLog(@"didReceiveMessage()");
};
- (void) socketIO:(SocketIO *)socket didReceiveJSON:(SocketIOPacket *)packet{
    NSLog(@"didReceiveJSON()");
};

-(void)creteChannel:(NSString *)channel{
    
    SocketIOCallback cb = ^(id argsData) {
        NSDictionary *response = argsData;
        // do something with response
        
        NSLog(@" === channel created ===  : %@",response);
    };
    NSArray *data = [[NSArray alloc] initWithObjects:@"notdol1",@"notdol2",@"notdol3",nil];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]   init];
    [dict setObject:data forKey:@"users"];
    [socketIO sendEvent:@"channel-create" withData:dict andAcknowledge:cb];
};

-(NSString *)getChannelList{
    
	xpushAppDelegate *appDelegate = (xpushAppDelegate *)[[UIApplication sharedApplication] delegate];
    StalkIOAppDelegate *stalk = appDelegate.stalk;
    _channelList = [stalk readChannel];
    
    
    SocketIOCallback cb = ^(id argsData) {
        NSDictionary *response = argsData;
        // do something with response
        
        NSLog(@" channel information received : %@",response);
        if([response[@"status"] isEqualToString:@"ok"]){
            //_channelList = [[NSArray alloc]initWithArray:response[@"result"]];
            //_channelList = [[NSMutableArray alloc]   init];
            NSArray *channelArr = [[NSArray alloc]initWithArray:response[@"result"]];
            for(NSDictionary *sub in channelArr){
                ChannelInfo *ch = [[ChannelInfo alloc] init];
                NSArray *keys =  [sub allKeys];
                NSLog(@" keys is  : %@", keys);
                for(NSString *key in keys){
                    NSDictionary *innerDic = [sub valueForKey:key];
                    ch.name = key;
                    NSLog(@" key is : %@", ch.name);
                    
                    NSLog(@"inner dic : %@", innerDic);
                    ch.id = [innerDic valueForKey:@"s"];
                    NSLog(@" id is : %@",ch.id);
                    ch.count = [innerDic valueForKey:@"c"];
                }
                
                if( [self isExistChannel:ch.name] == FALSE){
                    if( [stalk insertChannel:ch] == TRUE){
                        [_channelList addObject:ch];
                    }
                }
            }
            NSLog(@"to Data complete");
            
            //NSLog(@"channel : %@",channelList);
            //if(response[@"result"] == nil){
            //    NSLog(@"channel list is empty!!!");
            //}else{
                //_channelList = [[NSDictionary alloc] initWithDictionary:response[@"result"]];
            //}
        }
        
       // NSLog(@" channel list is  %@",_channelList);
    };
    
    [socketIO sendEvent:@"channel-list-active" withData:[[NSDictionary alloc]init]  andAcknowledge:cb];
    return @"test";
}


- (void) socketIO:(SocketIO *)socket onError:(NSError *)error
{
    NSLog(@"onError() %@", error);
}


- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error
{
    NSLog(@"socket.io disconnected. did error occur? %@", error);
}

# pragma mark -
/*
 - (void) viewDidUnload
 {
 [super viewDidUnload];
 }
 */
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
