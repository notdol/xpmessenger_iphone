//
//  XPushClient.h
//  XPush
//
//  Created by notdol on 3/8/14.
//  Copyright (c) 2014 com.notdol. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SocketIO.h"
#import "XPushChannel.h"
#import "ChannelInfo.h"

@protocol XPushSessionDelegate <NSObject>
@optional
- (void) socketConnected;
- (void) receivedNewChannel:(ChannelInfo *)chInfo;
- (void) receivedMessage:(NSString *)channel msgObj:(XPushMessage *)msgObj;
@end


@interface XPushClient : UIViewController <SocketIODelegate>
{
    SocketIO *socketIO;
    __weak id<XPushSessionDelegate> _delegate;
}

@property (nonatomic,strong) NSMutableArray *_channelList;

+(XPushClient*)getInstance;
-(void)setSessionServerUrl:(NSString *)url;
-(void)connect;
-(void)setDelegate:(id<XPushSessionDelegate>)delegate;
-(Boolean) isExistChannel:(NSString *)chNm;

-(void)setInformation:(NSString *)app userId:(NSString *)userId deviceId:(NSString*)deviceId token:(NSString *)token;
- (void) socketIODidConnect:(SocketIO *)socket;
-(XPushChannel *)connectToChannel:(NSString *)name;
- (void) socketIO:(SocketIO *)socket didReceiveMessage:(SocketIOPacket *)packet;
- (void) socketIO:(SocketIO *)socket didReceiveJSON:(SocketIOPacket *)packet;
- (void) socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet;

@end



